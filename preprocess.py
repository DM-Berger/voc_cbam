from argparse import ArgumentParser, Namespace
from dataclasses import dataclass
from enum import Enum
from pathlib import Path
from typing import Any, Callable, Dict, List, Optional, Sequence, Tuple, Type, Union, cast, no_type_check

import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import PIL
import pytest
import torch
from numpy import ndarray
from pandas import DataFrame, Series
from PIL import Image
from pytorch_lightning import LightningModule, Trainer, seed_everything
from pytorch_lightning.callbacks import Callback, EarlyStopping, LearningRateMonitor, ModelCheckpoint
from torch import Tensor
from torch.nn import Conv1d, LeakyReLU, Linear, Module, Sequential
from torch.optim import Adam
from torch.optim.optimizer import Optimizer
from torchmetrics.functional import accuracy
from torchvision.io import read_image
from torchvision.transforms import Resize
from torchvision.utils import save_image
from tqdm.contrib.concurrent import process_map
from typing_extensions import Literal

from loader import MainDataset


def ensure_dir(path: Path) -> Path:
    if not path.exists():
        path.mkdir(exist_ok=True, parents=True)
    return path


ROOT = Path(__file__).resolve().parent
OUTDIR = ensure_dir(ROOT / "VOC_Main/MainDataset")
TRAIN = ensure_dir(OUTDIR / "train")
VAL = ensure_dir(OUTDIR / "val")


def resize(args: Tuple[Path, str]) -> None:
    path, phase = args
    outdir = TRAIN if phase == "train" else VAL
    image = np.asarray(Image.open(path))
    image = cv2.resize(image, (224, 224))
    image = torch.from_numpy(image)
    save_image(image, outdir / path.name)


def preprocess_main_datasets() -> None:
    train = MainDataset(split="train")
    args = [(s[1], "train") for s in train.samples]
    process_map(resize, args)

    train = MainDataset(split="val")
    args = [(s[1], "val") for s in train.samples]
    process_map(resize, args)


if __name__ == "__main__":
    preprocess_main_datasets()
