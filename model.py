from typing import Any, Optional, Tuple, no_type_check

import torch
from pytorch_lightning import LightningModule
from torch import Tensor
from torch.nn import Linear
from torch.nn.functional import cross_entropy
from torch.optim.lr_scheduler import StepLR
from torch.utils.data import DataLoader
from torchmetrics.functional import accuracy
from torchvision.models import resnet18

from resnet import BasicBlock, ResidualNet


# from ResNet import ResidualNet
# This part of the code is implemented based on the official Lightning Module's github repository example https://rb.gy/s78ahn
class CbamMaps(LightningModule):
    def __init__(self, num_classes: int = 21, keep_attention: bool = False):
        super().__init__()
        # self.hparams = hparams_initial
        # self.save_hyperparameters()
        self.keep_attention = keep_attention
        self.model = ResidualNet("CBAM", depth=18, num_classes=num_classes, keep_attention=self.keep_attention)
        # self.train_dataset = train_dataset
        # self.val_dataset = val_dataset
        # self.test_dataset = test_dataset

    @no_type_check
    def forward(self, x: Tensor) -> Tensor:
        return self.model(x)

    @no_type_check
    def training_step(self, batch: Tuple[Tensor, Tensor], batch_idx: int) -> Tensor:
        return self.shared_step(batch, "train")

    @no_type_check
    def validation_step(self, batch: Tuple[Tensor, Tensor], batch_idx: int) -> None:
        self.shared_step(batch, "val")

    @no_type_check
    def test_step(self, batch: Tuple[Tensor, Tensor], batch_idx: int) -> None:
        self.shared_step(batch, "test")

    def on_predict_start(self) -> None:
        """Ensure we are getting all fmaps, attention components"""
        self.keep_attention = True
        self.model.keep_attention = True
        block_lists = [
            self.model.layer1_blocks,
            self.model.layer2_blocks,
            self.model.layer3_blocks,
            self.model.layer4_blocks,
        ]
        for module_list in block_lists:
            for block in module_list:
                if isinstance(block, BasicBlock):
                    block.return_attention = True

    def predict_step(
        self, batch: Tuple[Tensor, Tensor], batch_idx: int, dataloader_idx: Optional[int] = None
    ) -> Tuple[Tensor, Tensor, Tensor]:

        x = batch[0]
        # x = x.permute(0, 3, 1, 2).float()
        feature_maps, channel_attention_maps, spatial_attention_maps = self(x)[1:]
        return feature_maps, channel_attention_maps, spatial_attention_maps

    def shared_step(self, batch: Tuple[Tensor, Tensor], phase: str) -> Tensor:
        x, y = batch
        # x = x.permute(0, 3, 1, 2).float()
        if self.keep_attention:
            logits = self(x)[0]
        else:
            logits = self(x)
        y = y.squeeze()
        loss = cross_entropy(logits, y.long())
        acc = accuracy(logits, y)
        self.log(f"{phase}/loss", loss)
        self.log(f"{phase}/acc", acc, prog_bar=True)
        return loss

    def configure_optimizers(self) -> Any:
        optimizer = torch.optim.Adam(self.model.parameters(), lr=1e-4, weight_decay=1e-4)
        return [optimizer], [StepLR(optimizer, step_size=30, gamma=0.5)]

    # def train_dataloader(self) -> DataLoader:
    #     return DataLoader(self.train_dataset, batch_size=self.batch_size, num_workers=4, shuffle=True)

    # def val_dataloader(self) -> DataLoader:
    #     return DataLoader(self.val_dataset, batch_size=self.batch_size, num_workers=4)

    # def test_dataloader(self) -> DataLoader:
    #     return DataLoader(self.test_dataset, batch_size=self.batch_size, num_workers=4)


class ResNet18(LightningModule):
    def __init__(self, keep_attention: bool = False):
        super().__init__()
        # self.hparams = hparams_initial
        # self.save_hyperparameters()
        self.keep_attention = keep_attention
        self.model = resnet18(pretrained=True)
        self.model.fc = Linear(2048, 21)
        # self.train_dataset = train_dataset
        # self.val_dataset = val_dataset
        # self.test_dataset = test_dataset

    @no_type_check
    def forward(self, x: Tensor) -> Tensor:
        return self.model(x)

    @no_type_check
    def training_step(self, batch: Tuple[Tensor, Tensor], batch_idx: int) -> Tensor:
        return self.shared_step(batch, "train")

    @no_type_check
    def validation_step(self, batch: Tuple[Tensor, Tensor], batch_idx: int) -> None:
        self.shared_step(batch, "val")

    @no_type_check
    def test_step(self, batch: Tuple[Tensor, Tensor], batch_idx: int) -> None:
        self.shared_step(batch, "test")

    def on_predict_start(self) -> None:
        """Ensure we are getting all fmaps, attention components"""
        self.keep_attention = True
        self.model.keep_attention = True
        block_lists = [
            self.model.layer1_blocks,
            self.model.layer2_blocks,
            self.model.layer3_blocks,
            self.model.layer4_blocks,
        ]
        for module_list in block_lists:
            for block in module_list:
                if isinstance(block, BasicBlock):
                    block.return_attention = True

    def predict_step(
        self, batch: Tuple[Tensor, Tensor], batch_idx: int, dataloader_idx: Optional[int] = None
    ) -> Tuple[Tensor, Tensor, Tensor]:

        x = batch[0]
        # x = x.permute(0, 3, 1, 2).float()
        feature_maps, channel_attention_maps, spatial_attention_maps = self(x)[1:]
        return feature_maps, channel_attention_maps, spatial_attention_maps

    def shared_step(self, batch: Tuple[Tensor, Tensor], phase: str) -> Tensor:
        x, y = batch
        # x = x.permute(0, 3, 1, 2).float()
        if self.keep_attention:
            logits = self(x)[0]
        else:
            logits = self(x)
        y = y.squeeze()
        loss = cross_entropy(logits, y.long())
        acc = accuracy(logits, y)
        self.log(f"{phase}/loss", loss)
        self.log(f"{phase}/acc", acc, prog_bar=True)
        return loss

    def configure_optimizers(self) -> Any:
        optimizer = torch.optim.Adam(self.model.parameters(), lr=1e-5, weight_decay=1e-4)
        return [optimizer], [StepLR(optimizer, step_size=30, gamma=0.5)]
