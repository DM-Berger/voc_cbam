import sys
from argparse import ArgumentParser
from math import sqrt
from typing import Tuple

import matplotlib.pyplot as plt
import numpy as np
import torch
from matplotlib import pyplot as plt
from pytorch_lightning import LightningModule, Trainer
from skimage.metrics import mean_squared_error, normalized_root_mse
from skimage.metrics import structural_similarity as ssim
from torch.utils.data import DataLoader

from loader import DatasetWithoutAmbiguity, MainDataset, Segmentations
from model import CbamMaps

# from ResNet import ResidualNet
# This part of the code is implemented based on the official Lightning Module's github repository example https://rb.gy/s78ahn


def train() -> Tuple[LightningModule, Trainer]:
    source = "Main"
    CLASSES = [0, 3, 13, 18]

    # dataset = DatasetWithoutAmbiguity(multiclass=False)
    dataset = MainDataset("train", classes=CLASSES)  # aeroplane, boat, motorbike, train
    # dataset.count_classes()
    """
    class 0 (background): 327
    class 1 (aeroplane): 268
    class 2 (bicycle): 395
    class 3 (bird): 260
    class 4 (boat): 365
    class 5 (bottle): 213
    class 6 (bus): 590
    class 7 (car): 539
    class 8 (cat): 566
    class 9 (chair): 151
    class 10 (cow): 269
    class 11 (diningtable): 632
    class 12 (dog): 237
    class 13 (horse): 265
    class 14 (motorbike): 1994
    class 15 (person): 269
    class 16 (potted plant): 171
    class 17 (sheep): 257
    class 18 (sofa): 273
    class 19 (train): 290
    """
    # sys.exit()
    train_size = int(0.8 * len(dataset))
    train_dataset, val_dataset = torch.utils.data.random_split(dataset, [train_size, len(dataset) - train_size])
    train_loader = DataLoader(train_dataset, num_workers=2, shuffle=True, batch_size=16, drop_last=True)
    val_loader = DataLoader(val_dataset, num_workers=2, shuffle=False, batch_size=16, drop_last=True)

    # test_dataset = MainDataset("val")

    # else:  # Consturct Dataset based on availability of Segmentation Maps
    #     dataset = DatasetWithoutAmbiguity(multiclass=False)
    #     train_size = int(0.8 * len(dataset))
    #     train_dataset, test_dataset = torch.utils.data.random_split(dataset, [train_size, len(dataset) - train_size])
    #     val_size = int(0.2 * len(train_dataset))
    #     train_dataset, val_dataset = torch.utils.data.random_split(
    #         train_dataset, [len(train_dataset) - val_size, val_size]
    #     )

    print("No. of Train Samples %d" % len(train_dataset))
    print("No. of Val Samples %d" % len(val_dataset))
    # print("No. of Test Samples %d" % len(test_dataset))
    model = CbamMaps(num_classes=len(CLASSES), keep_attention=False)
    parser = ArgumentParser()
    parser = Trainer.add_argparse_args(parser)
    args = parser.parse_args()
    trainer: Trainer = Trainer.from_argparse_args(
        args,
        gpus=1,
        # enable_checkpointing=True,
        # limit_train_batches=1.0,
        min_epochs=1,
        max_epochs=50,
        auto_lr_find=True,
    )
    # results = trainer.tuner.lr_find(model, train_loader)
    # results.plot(suggest=True, show=True)
    # sys.exit()
    trainer.fit(model, train_loader, val_loader)
    # trainer.validate(model)
    # trainer.test(ckpt_path="best")
    return model, trainer


def visualize(model: LightningModule):
    model.to("cpu")

    segs_dataset = Segmentations()
    segs_dataset = DataLoader(segs_dataset, batch_size=32, num_workers=4)
    feature_map_idx = 0
    index = 0
    for batch in segs_dataset:
        category, images_np, seg = batch
        images = images_np.permute(0, 3, 1, 2).float()
        output, feature_maps, channel_attn_maps, spatial_attn_maps = model.forward(images.to("cpu"))
        # layout our images in a grid, with title info to say what we are plotting
        image_ids = np.arange(len(output))
        fig, axes = plt.subplots(nrows=len(image_ids), ncols=5)  # 20 categories total
        samples = []
        for image_idx in image_ids:
            feature_maps_spatial_attn = (
                spatial_attn_maps[feature_map_idx][image_idx] * feature_maps[feature_map_idx][image_idx]
            )
            all_maps_attn = (
                channel_attn_maps[feature_map_idx][image_idx].unsqueeze(1).unsqueeze(2)
                * spatial_attn_maps[feature_map_idx][image_idx]
            )
            fmap_attn_combined = all_maps_attn * feature_maps[feature_map_idx][image_idx]
            spatial_maps = spatial_attn_maps[feature_map_idx][image_idx].detach().numpy()
            samples.extend(
                [
                    ("Original", images_np[image_idx]),
                    ("Segmentation Map", seg[image_idx]),
                    (
                        "Feature Map",
                        feature_maps[feature_map_idx][image_idx][feature_map_idx].detach().numpy(),
                    ),
                    (
                        "Channel and Spatial Map",
                        all_maps_attn[feature_map_idx].detach().numpy().squeeze(),
                    ),
                    ("Spatial Map", spatial_maps.squeeze()),
                    # ("channel and Spatial Map * fmap",fmap_attn_combined[feature_map_idx].detach().numpy())
                ]
            )
            index += 1
            # Normalized Sum of Square Differences
            a = seg[image_idx].numpy()
            b = all_maps_attn[feature_map_idx].detach().numpy()
            # Correlation
            img1 = (a - np.mean(a)) / (np.std(a) * len(a))
            img2 = (b - np.mean(b)) / (np.std(b))
            print(index)
            # Normalized Euclidean distance
            distance = 0
            distance += np.square(img1 - img2)
            squared_dist = np.sqrt(distance)
            nor_dist = np.linalg.norm(squared_dist)
            print("Normalized Euclidean distance", nor_dist)
            im1 = (a - np.mean(a)) / (np.std(a) * len(a))
            im2 = (b - np.mean(b)) / (np.std(b))
            mse_const = mean_squared_error(im1, im2)
            print("mse", mse_const)
            rms = sqrt(mean_squared_error(a, b))
            print("rms", rms)
            ssim_const = ssim(a, b, data_range=a.max() - b.min())
            print("ssim", ssim_const)
            norm_root_mse_const = normalized_root_mse(im1, im2, normalization="euclidean")
            print("norm_root_mse", norm_root_mse_const)
            cm = np.corrcoef(a.flat, b.flat)
            r = cm[0, 1]
            print("correlation", r)
            # IOU,DICE
        ax: plt.Axes
        for (category, img), ax in zip(samples, axes.flat):
            ax.imshow(img)
            ax.set_title(category, fontsize=9)
            ax.set_xticklabels([])
            ax.set_yticklabels([])

        fig.set_size_inches(w=32, h=32)
        fig.tight_layout()
        plt.show()
        break


if __name__ == "__main__":
    torch.multiprocessing.set_sharing_strategy("file_system")
    train()
